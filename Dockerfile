FROM debian:11-slim AS build

ARG BRANCH
RUN echo "Building tor based off of: $BRANCH"

RUN apt update
RUN apt -y install git libevent-dev libssl-dev zlib1g-dev build-essential automake

RUN git clone --depth 1 --branch $BRANCH https://gitlab.torproject.org/tpo/core/tor.git
WORKDIR tor
RUN ./autogen.sh
RUN ./configure --sysconfdir=/etc --datadir=/var/lib --disable-asciidoc --disable-html-manual --disable-manpage
RUN make -j$(nproc)
RUN make install

FROM debian:11-slim

LABEL maintainer="Tor Project, Inc. <tor-dev@lists.torproject.org>"

COPY --from=build /usr/lib /usr/lib
COPY --from=build /usr/local/bin/tor* /usr/local/bin/

RUN adduser --disabled-password \
            --home "/home/tor" \
            --gecos "" \
            tor
USER tor

ENTRYPOINT ["tor"]
