# Tor Docker image

This image tracks the [Tor](https://gitlab.torproject.org/tpo/core/tor) repository. It automatically builds tags and branches based off of tags that are produced there.

## Which tag should you use?

If you are using this container in a production environment, we recommend using the most recent tagged version. Tagged versions will not change.

If you are using this container for development, then we suggest using the `main` tag. This tag is a rolling tag which updates on each commit, and ensures that you are always using the latest development version. 

## Get this image

The recommended way to get the Tor Docker Image is to pull the prebuilt image from our registry.

podman pull containers.torproject.org/micah/tor-package:main

To use a specific version, you can pull a versioned tag. You can view the list of available versions in the [container registry](https://gitlab.torproject.org/micah/tor-package/container_registry).

podman pull containers.torproject.org/micah/tor-package:[TAG]

You can also build the image yourself by cloning this repository, and executing the podman build command. Remember to replace the build argument in the example below with the branch/tag you wish to build APP, VERSION and OPERATING-SYSTEM path placeholders in the example command below with the correct values.

podman build --build-arg="BRANCH=tor-0.4.8.9" -t tor:0.4.8.9 .

## How to use this image

Create a `.torrc` file with the [values you would like](https://community.torproject.org/relay/setup/), and then run the image like this, adjusting the ports you need accordingly:

podman run -d --name tor -p 4443:443 -v./.torrc:/home/tor/.torrc containers.torproject.org/micah/tor-package:0.4.8.9

### Logging

The Docker image sends the container logs to stdout. To view the logs:

docker logs tor

## CI information

This project will build a docker image out of the tor source, using the `$BRANCH` environment variable to determine which branch/tag to build.

This project is triggered as a multi-project pipeline from micah/trigger-test. That project has a `.gitlab-ci.yml` trigger job that passes the `$CI_COMMIT_REF_NAME` from that pipeline to this pipeline as the `$REF` variable. The `$REF` variable is passed as a build argument to the docker image as `$BRANCH` to determine which branch to checkout from the tor source.

The `.gitlab-ci.yml` in this project additionally declares a `REF` variable set. This is set so that when this project has non-triggered push events, it will build the main branch. Because trigger variables [have the highest precedence](https://docs.gitlab.com/ee/ci/variables/index.html#cicd-variable-precedence), this variable will be overridden when a pipeline is triggered.

# TODO:
 - Multi-arch builds
 - Build with DCT (Docker Container Trust)
